# README #

### Prerequisites ###
* Angular CLI 11.0.4 or above
* Node v15.5.1 or above
* NPM v7.3.0 or above

### Installation Guide ###
* Clone or Download the Repository
* Navigate in the main folder "/blockchainvalidator"
* Launch command "npm install" to install all the dependencies and required packages
* Move into the folder "/blockchainvalidator/client"
* Launch command "ng serve -o" to let Angular compile, launch the project and open "http://localhost:4200"

### Installation Guide 2 ###
* Clone or Download the Repository
* Navigate in the main folder "/blockchainvalidator"
* Execute Script "1-INSTALL REQUIRED DEPENDENCIES.sh" to install all the Project Required Dependencies
* Execute Script "2-LAUNCH PROJECT SCRIPT.sh" to Launch the Project with Angular and open "http://localhost:4200"

### Who do I talk to? ###

* Cristiano Aprigliano 
* Email: cristiano.aprigliano@studio.unibo.it



