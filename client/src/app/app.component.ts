import { Component } from '@angular/core';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { VerifyService } from './verify.service';
import { Papa } from 'ngx-papaparse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';
  public curriculum_Array;
  public files: NgxFileDropEntry[] = [];

  constructor (private verifyService: VerifyService, private papa: Papa){
    
  }

  public verify(){
    console.log(this.verifyService.verify());
  }
 
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
 
      // Checks wether is a file or not
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
 
          // Here you can access the real file
          console.log(droppedFile.relativePath, file);

          this.papa.parse(file, {
            complete: result => {
              this.curriculum_Array = result;
              console.log(this.curriculum_Array);
            },
              header: true,
              skipEmptyLines: true,
          });
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event){
    console.log(event);
  }
 
  public fileLeave(event){
    console.log(event);
  }
  
  headers = ["CID", "codice Oggetto", "Definizione oggetto", "Tipo Corso", "N. ore", "Inizio", "Fine"];
}