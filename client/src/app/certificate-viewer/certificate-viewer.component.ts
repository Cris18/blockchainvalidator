import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-viewer',
  templateUrl: './certificate-viewer.component.html',
  styleUrls: ['./certificate-viewer.component.css']
})
export class FileViewerComponent implements OnInit {

  @Input('dataTable') dataTable;
  
  constructor() { }

  ngOnInit(): void {
    console.log(this.dataTable);
  }
};
