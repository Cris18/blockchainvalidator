import { Injectable } from '@angular/core';
//import { readFile } from 'fs';

@Injectable({
  providedIn: 'root'
})
export class VerifyService {

  constructor() { }

  public verify() { 
     if(Math.floor((Math.random() * 10) + 1) % 2 == 0){
       return this.successVerification();
     } else {
       return this.failedVerification();
     }
  }

  public successVerification() {
    console.log("EVEN");
    document.getElementById("verificationResultSuccess").style.display = "block";
    (document.getElementById("verifyButton") as HTMLInputElement).disabled = true;
  }

  public failedVerification() {
    console.log("ODD");
    document.getElementById("verificationResultFailed").style.display = "block";
    (document.getElementById("verifyButton") as HTMLInputElement).disabled = true;
  }
}
